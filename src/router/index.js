import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Frist from '@/components/Frist'
import College from '@/components/College'
import CollegeAdd from '@/components/CollegeAdd'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'College',
      component: College
    },
    {
      path: '/CollegeAdd',
      name: 'CollegeAdd',
      component: CollegeAdd
    },
    {
      path: '/College',
      name: 'College',
      component: College
    }
  ]
})
